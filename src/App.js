import React from "react";
import "bootstrap/dist/css/bootstrap.min.css";
import styled from "styled-components";

import { Container, Nav } from "react-bootstrap";

import { BrowserRouter as Router, Route } from "react-router-dom";
import { withRouter } from "react-router";

import SearchMoviePage from "./container/SearchMovie";
import FavouriteMoviePage from "./container/FavouriteMovie";

const NavCustom = styled(Nav)`
  margin: 20px;
`;

const Header = (props) => {
  const { location } = props;
  return (
    <NavCustom variant="tabs" defaultActiveKey={location.pathname}>
      <Nav.Item>
        <Nav.Link href="/search-movie" eventKey="/search-movie">
          Search Movie
        </Nav.Link>
      </Nav.Item>
      <Nav.Item>
        <Nav.Link href="/favourite-movie" eventKey="/favourite-movie">
          Favourite Movie
        </Nav.Link>
      </Nav.Item>
    </NavCustom>
  );
};
const HeaderWithRouter = withRouter(Header);

const SearchMovie = () => <SearchMoviePage />;
const FavouriteMovie = () => <FavouriteMoviePage />;

function App() {
  return (
    <Router>
      <Container>
        <HeaderWithRouter />
        <Route path="/search-movie" exact component={SearchMovie} />
        <Route path="/favourite-movie" exact component={FavouriteMovie} />
      </Container>
    </Router>
  );
}

export default App;
