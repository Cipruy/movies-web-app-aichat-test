import { createSlice } from "@reduxjs/toolkit";

export const moviesSlice = createSlice({
  name: "movies",
  initialState: {
    totalResult: 0,
  },
  reducers: {
    totalMovies: (state, action) => {
      state.totalResult = action.payload;
    },
  },
});

export const { totalMovies } = moviesSlice.actions;

export const selectState = (state) => state.movies.totalResult;

export default moviesSlice.reducer;
