import React, { useState } from "react";
import axios from "axios";

import PropTypes from "prop-types";
import AlertDismissablePage2 from "../components/AlertDismissablePage2";
import ModalDetailFilm from "../components/ModalDetailFilm";

import { Table } from "react-bootstrap";

const FavouriteMovie = (props) => {
  const [modalShow, setModal] = useState(false);
  const [dataFavourite] = useState(localStorage.getItem("favouriteMovie"));
  const [detailMovie, setDetailMovie] = useState({});

  const dataFavouriteMovie = JSON.parse(dataFavourite);

  const handleModal = (event) => {
    setModal(!modalShow);
    if (event !== undefined) {
      const url = `http://www.omdbapi.com/?i=${event.target.id}&apikey=d1a84f5b`;
      axios.post(url).then((res) => {
        if (res.data.Response === "False") {
          console.log(res.data.Error);
        } else {
          setDetailMovie(res.data);
        }
      });
    } else {
      setModal(!modalShow);
    }
  };

  const removeFavourite = () => {
    localStorage.removeItem("favouriteMovie");
    window.location.reload();
  };

  return (
    <div>
      <AlertDismissablePage2 />
      <ModalDetailFilm
        show={modalShow}
        onHide={handleModal}
        data={detailMovie}
      />
      {dataFavourite === null ? (
        <Table
          striped
          bordered
          hover
          variant="dark"
          style={{ marginTop: "20px" }}
        >
          <thead>
            <tr>
              <th>Title</th>
              <th>Year</th>
              <th>Language</th>
              <th>Remove From Favourite</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td colSpan="4" style={{ textAlign: "center" }}>
                Please add your favourite movie first
              </td>
            </tr>
          </tbody>
        </Table>
      ) : (
        <Table
          striped
          bordered
          hover
          variant="dark"
          style={{ marginTop: "20px" }}
        >
          <thead>
            <tr>
              <th>Title</th>
              <th>Year</th>
              <th>Language</th>
              <th>Remove From Favourite</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td
                id={dataFavouriteMovie.imdbID}
                style={{ color: "red", cursor: "pointer" }}
                onClick={handleModal}
              >
                {dataFavouriteMovie.Title}
              </td>
              <td>{dataFavouriteMovie.Year}</td>
              <td>{dataFavouriteMovie.Language}</td>
              <td>
                <i
                  className="material-icons"
                  style={{ color: "red", cursor: "pointer" }}
                  onClick={removeFavourite}
                >
                  star
                </i>
              </td>
            </tr>
          </tbody>
        </Table>
      )}
    </div>
  );
};

FavouriteMovie.propTypes = {
  history: PropTypes.func,
};

export default FavouriteMovie;
