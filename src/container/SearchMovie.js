import React, { useState } from "react";
import axios from "axios";
import styled from "styled-components";

import AlertDismissablePage1 from "../components/AlertDismissablePage1";

import ModalDetailFilm from "../components/ModalDetailFilm";

import { Form, FormControl, Button, Table, Row, Col } from "react-bootstrap";

import { useSelector, useDispatch } from "react-redux";
import { totalMovies, selectState } from "./movieSlice";

const FormCustom = styled(Form)`
  margin-top: 20px;
  margin-bottom: 20px;
`;

const SearchMovie = () => {
  const total = useSelector(selectState);
  const dispatch = useDispatch();

  const [modalShow, setModal] = useState(false);
  const [searchValue, setSearchValue] = useState("");
  const [totalResult, setTotalResult] = useState(0);
  const [dataMovies, setDataMovies] = useState([]);
  const [detailMovie, setDetailMovie] = useState([]);
  const [favourite] = useState(false);

  const handleModal = (event) => {
    setModal(!modalShow);
    if (event !== undefined) {
      const url = `http://www.omdbapi.com/?i=${event.target.id}&apikey=d1a84f5b`;
      axios.post(url).then((res) => {
        if (res.data.Response === "False") {
          console.log(res.data.Error);
        } else {
          setDetailMovie(res.data);
        }
      });
    } else {
      setModal(!modalShow);
    }
  };

  const handleChangeSearch = (event) => {
    setSearchValue(event.target.value);
  };

  const handleToggleFavourite = (event) => {
    let updatedList = dataMovies.map((obj) => {
      if (obj.imdbID === event.target.id) {
        return Object.assign({}, obj, {
          favourite: !favourite,
        });
      }
      return obj;
    });
    setDataMovies(updatedList);
    const url = `http://www.omdbapi.com/?i=${event.target.id}&apikey=d1a84f5b`;
    axios.post(url).then((res) => {
      if (res.data.Response === "False") {
        alert(res.data.Error);
      } else {
        localStorage.setItem("favouriteMovie", JSON.stringify(res.data));
      }
    });
  };

  const handleSubmitSearch = () => {
    const url = `http://www.omdbapi.com/?s=${searchValue}&apikey=d1a84f5b`;
    axios.post(url).then((res) => {
      if (res.data.Response === "False") {
        alert(res.data.Error);
      } else {
        setDataMovies(res.data.Search);
        setTotalResult(res.data.totalResults);
      }
    });
  };

  const Title = dataMovies.map((row, key) => (
    <tr key={key}>
      <td
        id={row.imdbID}
        onClick={handleModal}
        style={{ color: "red", cursor: "pointer" }}
      >
        {row.Title}
      </td>
      <td>{row.Year}</td>
      <td>{row.imdbID}</td>
      <td>
        {!row.favourite ? (
          <i
            id={row.imdbID}
            className="material-icons"
            style={{ color: "red", cursor: "pointer" }}
            onClick={handleToggleFavourite}
          >
            star_border
          </i>
        ) : (
          <i
            id={row.imdbID}
            className="material-icons"
            style={{ color: "red", cursor: "pointer" }}
            onClick={handleToggleFavourite}
          >
            star
          </i>
        )}
      </td>
    </tr>
  ));

  return (
    <div>
      <AlertDismissablePage1 />
      <Row>
        <Col sm={8}>
          <FormCustom inline>
            <FormControl
              name="searchValue"
              type="text"
              placeholder="Enter Movie Title Here..."
              className="mr-sm-2"
              value={searchValue}
              onChange={handleChangeSearch}
            />
            <Button
              variant="outline-primary"
              onClick={() =>
                dispatch(totalMovies(Number(totalResult), handleSubmitSearch()))
              }
            >
              Search
            </Button>
          </FormCustom>
        </Col>
        <Col sm={4} style={{ display: "flex", alignItems: "center" }}>
          <span>
            TOTAL RESULT: {totalResult === 0 ? total : totalResult} Movies
          </span>
        </Col>
      </Row>
      <ModalDetailFilm
        show={modalShow}
        onHide={handleModal}
        data={detailMovie}
      />
      {dataMovies.length === 0 ? (
        <Table
          striped
          bordered
          hover
          variant="dark"
          style={{ marginTop: "20px" }}
        >
          <thead>
            <tr>
              <th>Title</th>
              <th>Year</th>
              <th>IMDb ID</th>
              <th>Add to Favourite</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td colSpan="4" style={{ textAlign: "center" }}>
                Please search by movie title first
              </td>
            </tr>
          </tbody>
        </Table>
      ) : (
        <Table
          striped
          bordered
          hover
          variant="dark"
          style={{ marginTop: "20px" }}
        >
          <thead>
            <tr>
              <th>Title</th>
              <th>Year</th>
              <th>IMDb ID</th>
              <th>Add to Favourite</th>
            </tr>
          </thead>
          <tbody>{Title}</tbody>
        </Table>
      )}
    </div>
  );
};

export default SearchMovie;
