import { configureStore } from "@reduxjs/toolkit";
import moviesReducer from "../container/movieSlice";

export default configureStore({
  reducer: {
    movies: moviesReducer,
  },
});
