import React from "react";
import { Modal, Image, Button, Row, Col } from "react-bootstrap";

function ModalDetailFilm(props) {
  return (
    <Modal
      {...props}
      size="lg"
      aria-labelledby="contained-modal-title-vcenter"
      centered
    >
      <Modal.Header closeButton>
        <Modal.Title id="contained-modal-title-vcenter">
          Detail Film
        </Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <Row>
          <Col>
            <Image src={props.data.Poster} rounded />
          </Col>
          <Col>
            <h5 style={{ marginTop: "20px", marginBottom: "20px" }}>
              Title : {props.data.Title}
            </h5>
            <p>Writer: {props.data.Writer}</p>
            <p>Actors: {props.data.Actors}</p>
            <p>Year : {props.data.Year}</p>
            <p>IMDb Rating : {props.data.imdbRating}</p>
            <p>Language : {props.data.Language}</p>
          </Col>
        </Row>
      </Modal.Body>
      <Modal.Footer>
        <Button onClick={props.onHide}>Close</Button>
      </Modal.Footer>
    </Modal>
  );
}

export default ModalDetailFilm;
