import React, { useState } from "react";
import { Alert, Button } from "react-bootstrap";

function AlertDismissiblePage2() {
  const [show, setShow] = useState(true);

  return (
    <div>
      <Alert show={show} variant="primary">
        <Alert.Heading>
          Hello friends, this web app for AiChat coding test
        </Alert.Heading>
        <p>This Page is for peek your favourite movie</p>
        <p>Please, add or remove your favourite movies</p>
        <hr />
        <div className="d-flex justify-content-end">
          <Button onClick={() => setShow(false)} variant="outline-primary">
            Close
          </Button>
        </div>
      </Alert>

      {!show && (
        <Button onClick={() => setShow(true)} variant="outline-primary">
          Show Alert
        </Button>
      )}
    </div>
  );
}

export default AlertDismissiblePage2;
